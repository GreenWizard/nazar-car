var gulp = require('gulp');
var util = require('util');
var clear = require('clear');
var spawn = require('child_process').spawn;

gulp.task('jest', function(cb) {
  clear();
  spawn('./node_modules/.bin/jest', [], {stdio: "inherit"}).on('close', _ => cb(null));
});

gulp.task('default', function() {
    gulp.watch(['src/**/*.js'], { debounceDelay: 500 }, gulp.series('jest'));
});
