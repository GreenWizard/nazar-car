import createHTTP from './defaultHttp.js';

class CRCCar {
  constructor(http) {
    this._http = ('string' === typeof(http)) ? createHTTP('http://' + http) : http;
  }

  ping() {
    return this._http
      .get('/ping')
      .then(response => response.json())
      .then(response => {
        if(!response.online) throw new Error('not ok');
        return(response);
      });
  }

  send(cmd) {
    return this._http.get(`/cmd?command=${cmd}&power=${this._power}&duration=${this._duration}`)
      .then(response => response.json())
      .then(({ data }) => {
        if(!data.online) throw new Error('not ok');
        return(data);
      });
  }

  power(value) {
    this._power = value;
  }

  duration(value) {
    this._duration = value;
  }
}

class CRCCarDev {
  ping() {
    return Promise.resolve(true);
  }

  send(cmd) {
    console.log('command: ', cmd);
    return Promise.resolve('dev');
  }

  power(value) {
    this._power = value;
    console.log('power: ', value);
  }

  duration(value) {
    this._duration = value;
    console.log('duration: ', value);
  }
}

const devMode = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

export default (devMode ? CRCCarDev : CRCCar);
export { CRCCar };
