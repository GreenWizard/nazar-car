import createHTTP from './defaultHttp.js';

class CRCTermopod {
  constructor(http) {
    this._http = ('string' === typeof(http)) ? createHTTP('http://' + http) : http;
  }

  open(closeDelay = 5000) {
    return this._http.get(`/open?close_delay=${closeDelay}`);
  }

  close() {
    return this._http.get('/close');
  }
}

export default CRCTermopod;
