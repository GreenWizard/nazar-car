import axios from 'axios';

function serialize(val) {
  if('object' !== typeof(val))
    return val;

  return JSON.stringify(val)
    .replace(
      /[\u007F-\uFFFF]/g,
      function(chr) {
        return "\\u" + ("0000" + chr.charCodeAt(0).toString(16)).substr(-4)
      }
    );
}

export const axiosInstance = axios.create({
  withCredentials: true,
});

export default function (server) {
  return {
    get: (url, values, opt) => {
      const data = new FormData();
      for (var name in values) {
        data.set(name, serialize(values[name]));
      }

      return window.fetch(server + url);
    }
  }
};
