import React from 'react';

import { Row, Col } from 'react-bootstrap';
import Slider from 'react-input-slider';

import GAUGE_IMG from 'gauge.png';

const SLIDER_STYLE = {
  track: {
    width: '100%',
    height: '3rem',
    backgroundColor: 'none',
    backgroundImage: `url(${GAUGE_IMG})`,
    backgroundSize: '100% 100%',
    backgroundRepeat: 'no-repeat',
    border: '1px solid blue',
  },

  active: {
    backgroundColor: 'silver',
    opacity: 0.6,
  },

  thumb: {
    height: '2.5rem',
    width: 3,
    borderRadius: 0,
    backgroundColor: '#000',
    cursor: 'pointer',
    boxSizing: 'border-box'
  }
};

const DURATION_SLIDER_STYLE = {
  track: {
    width: '100%',
    height: '2.5rem',
  },
  thumb: {
    width: '2.5rem',
    height: '2.5rem',
  }
};

class CarSettings extends React.Component {
  render() {
    return (
    <>
      <Row className="controls">
        <Col className="power-area">
          <Slider
            axis="x" xstep={1} xmin={0} xmax={100}
            x={100 - this.props.power} xreverse
            onChange={({ x }) => this.props.onPowerChanged(100 - x)}
            styles={SLIDER_STYLE}
          />
          <div className="text-center">
            {this.props.power.toFixed(0)}%
          </div>
        </Col>
      </Row>
      <Row className="duration-area">
        <Col className="text-nowrap my-auto">
          Длительность команд
        </Col>
      </Row>
      <Row>
        <Col>
          <Slider
            axis="x" xstep={25} xmin={25} xmax={1000}
            x={this.props.duration}
            onChange={({ x }) => this.props.onDurationChanged(x)}
            styles={DURATION_SLIDER_STYLE}
          />
          <div className="text-center">
            {this.props.duration.toFixed(0)} мс
          </div>
        </Col>
      </Row>
    </>
    );
  }
}

export default CarSettings;
