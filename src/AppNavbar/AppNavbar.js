import React from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import { Navbar, Nav } from 'react-bootstrap';

const Header = props => {
  const { location } = props;
  return (
    <Navbar bg="light" variant="light" className="AppNavbar">
      <Nav activeKey={location.pathname}>
        <Nav.Link as={NavLink} exact to="/">Машинка</Nav.Link>
        <Nav.Link as={NavLink} exact to="/termopod">Термопод</Nav.Link>
      </Nav>
    </Navbar>
  );
};
const AppNavbar = withRouter(Header);

export default AppNavbar;
