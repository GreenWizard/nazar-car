import React from 'react';

import CarSettings from 'CarSettings.js';
import CarControls from 'CarControls/CarControls.js';

import Polling from 'Utils/Polling.js';
import LoadingSpinner from 'Utils/LoadingSpinner.js';
import { withState } from 'Utils/withState.js';

@withState
class CarPage extends React.Component {
  state = {
    online: false,
  };

  onConnected = () => {
    this.State({ online: true });
  }

  onDisconnected = () => {
    this.State({ online: false });
  }

  checkConnection = () => {
    this.props.car.ping().then(this.onConnected, this.onDisconnected);
  }

  onPowerChanged = x => this.props.update({ power: x });
  onDurationChanged = x => this.props.update({ duration: x });

  onAction = (cmd) => {
    this.props.car.send(cmd);
  }

  render() {
    return (
    <>
      <Polling update={this.checkConnection} period={process.env.REACT_APP_PING_DELAY} />
      {this.state.online ? (
        <>
          <CarSettings
            power={this.props.power}
            duration={this.props.duration}
            onPowerChanged={this.onPowerChanged}
            onDurationChanged={this.onDurationChanged}
          />

          <CarControls
            duration={this.props.duration}
            onAction={this.onAction}
          />
        </>
      ) : (
        <LoadingSpinner />
      )}
    </>
    );
  }
}

export default CarPage;
