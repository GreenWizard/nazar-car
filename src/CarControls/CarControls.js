import React from 'react';
import { Row, Table } from 'react-bootstrap';

import { withState } from 'Utils/withState.js';

import ROTATE_LEFT_IMG from './rotate-left.png';
import ROTATE_RIGHT_IMG from './rotate-right.png';

import MOVE_FORWARD_IMG from './move-forward.png';
import MOVE_BACKWARD_IMG from './move-backward.png';

@withState
class CarControls extends React.Component {
  state = {
    activeAction: null,
  };
  _timer = null;

  delayedResetButton() {
    clearTimeout(this._timer);
    this._timer = setTimeout(() => {
      this.State({ activeAction: null });
    }, this.props.duration);
  }

  action = (activeAction) => {
    this.State({ activeAction });
    this.props.onAction(activeAction);
    this.delayedResetButton();
  }

  rotateLeft = () => this.action('L');
  rotateRight = () => this.action('R');
  moveForward = () => this.action('F');
  moveBackward = () => this.action('B');

  componentWillUnmount() {
    clearTimeout(this._timer);
  }

  render() {
    const { activeAction } = this.state;

    return (
    <>
      <Row className="control-area">
        <Table className="control-table my-auto">
          <tbody>
            <tr>
              <td rowSpan={2}>
                <img alt="" draggable={false}
                  src={ROTATE_LEFT_IMG}
                  className={"control-button rotate-left " + ((activeAction !== 'L') || 'active')}
                  onMouseDown={this.rotateLeft}
                />
              </td>

              <td>
                <img alt="" draggable={false}
                  src={MOVE_FORWARD_IMG}
                  className={"control-button move-forward " + ((activeAction !== 'F') || 'active')}
                  onMouseDown={this.moveForward}
                />
              </td>
              <td rowSpan={2}>
                <img alt="" draggable={false}
                  src={ROTATE_RIGHT_IMG}
                  className={"control-button rotate-right " + ((activeAction !== 'R') || 'active')}
                  onMouseDown={this.rotateRight}
                />
              </td>
            </tr>
            <tr>
              <td>
                <img alt="" draggable={false}
                  src={MOVE_BACKWARD_IMG}
                  className={"control-button move-forward " + ((activeAction !== 'B') || 'active')}
                  onMouseDown={this.moveBackward}
                />
              </td>
            </tr>
          </tbody>
        </Table>
      </Row>
    </>
    );
  }
}

export default CarControls;
