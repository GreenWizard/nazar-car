import React from 'react';
import './App.less';

import { Switch, Route, Redirect } from 'react-router-dom';
import { Container } from 'react-bootstrap';

import CRCCar from 'Core/CRCCar.js';
import CRCTermopod from 'Core/CRCTermopod.js';

import HttpErrorsPopup from 'Utils/HttpErrorsPopup.js';
import AppNavbar from 'AppNavbar/AppNavbar.js';

import CarPage from 'CarPage.js';
import TermopodPage from 'TermopodPage.js';

import { withState } from 'Utils/withState.js';

@withState
class App extends React.Component {
  constructor(p) {
    super(p);

    this.state = {
      termopod: new CRCTermopod('192.168.31.201'),
      closeDelay: 250,

      car: new CRCCar('192.168.31.200'),
      power: 100,
      duration: 200,
    };

    const car = this.state.car;
    car.duration(this.state.duration);
    car.power(this.state.power);
  }

  componentDidUpdate() {
    const { car, power, duration } = this.state;
    car.duration(duration);
    car.power(power);
  }

  render() {
    return (
    <>
      <HttpErrorsPopup />
      <Container className="main-container">
        <AppNavbar />

        <Switch>
          <Route path="/" exact render={() => (
            <CarPage
              car={this.state.car}
              power={this.state.power}
              duration={this.state.duration}
              update={x => this.State(x)}
            />
          )} />

          <Route path="/termopod" exact render={() => (
            <TermopodPage
              termopod={this.state.termopod}
              closeDelay={this.state.closeDelay}
              onCloseDelayChanged={closeDelay => this.State({ closeDelay })} />
          )} />

          <Route render={() => <Redirect to="/" />} />
        </Switch>
      </Container>
    </>
    );
  }
}

export default App;
