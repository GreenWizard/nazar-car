import React from 'react';

import { Row, Col } from 'react-bootstrap';
import Slider from 'react-input-slider';

import valve from './valve.png';
import { withState } from 'Utils/withState.js';

const SLIDER_STYLE = {
  track: {
    width: '100%',
    height: '2.5rem',
  },
  thumb: {
    width: '2.5rem',
    height: '2.5rem',
  }
};

@withState
class TermopodPage extends React.Component {
  state = {
    openned: false
  };
  _timer = null;

  delayedReset() {
    clearTimeout(this._timer);
    this._timer = setTimeout(() => {
      this.closeValve();
    }, this.props.closeDelay);
  }

  componentWillUnmount() {
    this.closeValve();
  }

  closeValve = () => {
    clearTimeout(this._timer);
    this.props.termopod.close();
    this.State({ openned: false });
  }

  toggleValve = () => {
    this.State(({ openned }) => {
      if(openned) {
        clearTimeout(this._timer);
        this.props.termopod.close();
      } else {
        this.props.termopod.open(this.props.closeDelay);
        this.delayedReset();
      }
      return { openned: !openned };
    });
  }

  render() {
    return (
    <>
      <Row className="pb-3">
        <Col>
          <div className="text-nowrap my-auto">
            Длительность:
          </div>
          <Slider
            axis="x" xstep={25} xmin={25} xmax={10000}
            x={this.props.closeDelay}
            onChange={({ x }) => this.props.onCloseDelayChanged(x)}
            styles={SLIDER_STYLE}
          />
          <div className="text-center">
            {this.props.closeDelay.toFixed(0)} мс
          </div>
        </Col>
      </Row>

      <Row className="termopod">
        <img
          alt="" src={valve}
          className={"control-button w-50" + (this.state.openned ? ' active' : '')}
          onMouseDown={this.toggleValve}
          draggable={false}
        />
      </Row>
    </>
    );
  }
}

export default TermopodPage;
