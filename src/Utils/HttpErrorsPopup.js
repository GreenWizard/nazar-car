import React from 'react';
import './HttpErrorsPopup.less';

import { axiosInstance } from 'Core/defaultHttp.js';

class HttpErrorsPopup extends React.Component {
  state = {
    title: null,
    msg: null,
    timestamp: null
  };
  timer = null;

  clearTimer = () => {
    this.timer && clearTimeout(this.timer);
    this.timer = null;
  }

  resetTimer = () => {
    this.clearTimer();
    const period = this.props.period ? (0 + this.props.period) : (30 * 1000);
    this.timer = setTimeout(this.hide, period);
  }

  hide = () => {
    this.clearTimer();
    this.State({ title: null, msg: null, timestamp: null })
  }

  componentDidMount() {
    axiosInstance.interceptors.response.use(null, error => {
      const { status, statusText } = error.response;
      this.State({
        title: 'HTTP error #' + status,
        msg: statusText,
        timestamp: Date.now()
      });

      return Promise.reject(error);
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if(this.state.timestamp !== prevState.timestamp) {
      this.resetTimer();
    }
  }

  componentWillUnmount() {
    this.clearTimer();
  }

  render() {
    if(!this.state.msg) return null;

    return (
      <div id="HttpErrorsPopup">
        <div onClick={this.hide}>
          <div className="error-title">{this.state.title}</div>
          <div className="error-msg">{this.state.msg}</div>
        </div>
      </div>
    );
  }
}

export default HttpErrorsPopup;
