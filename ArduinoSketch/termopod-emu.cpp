#include <iostream>
//////////////////////////
const int LOW = -1;
const int HIGH = 1;
const int OUTPUT = 1;

void pinMode(int pin, int mode) {
//  std::cout << "Pin mode for " << pin << " set to " << mode << std::endl;
}

int _time = 0;
void timestamp() {
  std::cout << _time << ": ";
}

int millis() {
  return _time;
}

bool isBlocked = false;
int lastBlockPin = LOW;
int unblockTime = -1000;
bool isOpen = true;

void digitalWrite(int pin, int value) {
  if(16 == pin) {
    bool switched = lastBlockPin != value;
    if(switched) {
      if(HIGH == value) {
        isBlocked = !(5000 < (millis() - unblockTime));
        unblockTime = millis();
        timestamp();
        std::cout << (isBlocked ? "block" : "unblock") << " walve." << std::endl;
      }
      lastBlockPin = value;
    }
    return;
  }

  bool open = LOW == value;
  if(isOpen != open) {
    isOpen = open;
    timestamp();
    std::cout << (open ? "open" : "close") << " walve." << std::endl;
  }
}

void analogWrite(int pin, float value) {
  std::cout << "analogWrite " << pin << " <- " << value << std::endl;
}

void delay(int ms) {
  _time += ms;
}

#define LOCAL_RUN
#include "termopod-sketch.h"

void reset() {
  std::cout << "----------------------------" << std::endl;
  _time = 0;
  isBlocked = false;
  lastBlockPin = LOW;
  isOpen = true;
  setup();
  std::cout << "setup() done" << std::endl;
  loop();
}

void run(int delta) {
  int targetT = _time + delta;
  while(_time < targetT) {
    _time += 1;
    loop();
  }
}

int main() {
  reset();
  run(5000);
  openRequest(100);
  run(1500);
  openRequest(1000);
  run(50);
  closeRequest();
  run(5000);

  return 0;
}
