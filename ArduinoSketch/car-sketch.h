#ifndef LOCAL_RUN
#include <ESP8266WiFi.h>
#include <wificlient.h>
#include <ESP8266WebServer.h>
#endif
///////////////////////////////////
struct TMotor {
private:
  int _pinA, _pinB, _pinSpeed;
public:
  TMotor(int A, int B, int speed): _pinA(A), _pinB(B), _pinSpeed(speed) {}

  void stop() {
    digitalWrite(_pinA, LOW);
    digitalWrite(_pinB, LOW);
    analogWrite(_pinSpeed, 0);
  }

  void run(bool forward, int power) { // power 0-100%
    digitalWrite(_pinA, forward ? LOW : HIGH);
    digitalWrite(_pinB, forward ? HIGH : LOW);

    int speed = 10 * power; // 1024 / 100 ~= 10
    analogWrite(_pinSpeed, speed);
  }

  void setup() {
    pinMode(_pinA, OUTPUT);
    pinMode(_pinB, OUTPUT);
    pinMode(_pinSpeed, OUTPUT);
  }
};

////////////////////////////////////
unsigned long stopAction = 0;

int power = 0;
const int speed_Coeff = 3;

#ifndef LOCAL_RUN
IPAddress network_ip(192,168,31,200);
IPAddress network_gateway(192,168,31,1);
IPAddress network_subnet(255,255,255,0);

const char* WiFi_login = "Xiaomi";
const char* WiFi_password = "19973211";

ESP8266WebServer server(80);

void WIFIinit() {
  WiFi.mode(WIFI_STA);
  byte tries = 51;
  WiFi.begin(WiFi_login, WiFi_password);
  while (--tries && WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();

  if (WiFi.status() != WL_CONNECTED) {
    StartAPMode();
  } else {
    WiFi.config(network_ip, network_gateway, network_subnet);
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
  }
}

void StartAPMode() {
  WiFi.disconnect();
  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(network_ip, network_ip, network_subnet);
  WiFi.softAP(WiFi_login, WiFi_password);

  Serial.print("AP IP address: ");
  Serial.println(WiFi.softAPIP());
}
#endif

TMotor leftMotor(2, 0, 12), rightMotor(15, 13, 14);

void setup() {
  leftMotor.setup();
  rightMotor.setup();

#ifndef LOCAL_RUN
  Serial.begin(115200);

  WIFIinit();
  wifi_set_sleep_type(NONE_SLEEP_T);

  // Starting WEB-server
  server.on("/", HTTP_POST, HTTP_handleAny);
  server.on("/", HTTP_GET, HTTP_handleAny);

  server.on("/cmd", HTTP_POST, HTTP_handleCommand);
  server.on("/cmd", HTTP_GET, HTTP_handleCommand);

  server.on("/ping", HTTP_POST, HTTP_handlePing);
  server.on("/ping", HTTP_GET, HTTP_handlePing);

  server.onNotFound(HTTP_handleAny);
  server.begin();
#endif
}

void goAhead() {
  leftMotor.run(true, power);
  rightMotor.run(true, power);
}

void goBack() {
  leftMotor.run(false, power);
  rightMotor.run(false, power);
}

void goRight() {
  leftMotor.run(true, power);
  rightMotor.run(false, power);
}

void goLeft() {
  leftMotor.run(false, power);
  rightMotor.run(true, power);
}

void stopRobot() {
  leftMotor.stop();
  rightMotor.stop();
}

void loop() {
#ifndef LOCAL_RUN
  server.handleClient();

  if((0 < stopAction) && (stopAction < millis())) {
    stopRobot();
    stopAction = 0;
  }
  delay(1);
#endif
}

#ifndef LOCAL_RUN
void HTTP_handleCommand(void) {
  power = server.arg("power").toInt();
  stopAction = millis() + server.arg("duration").toInt();

  String s = server.arg("command");

  if (s == "F") goAhead();
  if (s == "B") goBack();
  if (s == "L") goLeft();
  if (s == "R") goRight();

  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.send(200, "application/json", "{\"online\":true}");
}

void HTTP_handlePing(void) {
  String s = "{";
  s += "\"online\":true,";
  s += "\"power\":" + String(power);
  s += "}";

  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.send(200, "application/json", s);
}

void HTTP_handleAny(void) {
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.send(200, "application/json", "{\"online\":true}");
}
#endif
