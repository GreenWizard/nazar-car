#ifndef LOCAL_RUN
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#endif

// Wi-Fi
const char* WiFi_login = "Xiaomi";
const char* WiFi_password = "19973211";

// Тайминги
// Время автозакрытия
int CLOSE_DELAY = 5 * 1000;
// Время блокировки
int BLOCK_DELAY = 5 * 1000;

// Время удержания пина блокировки
int BLOCKING_HOLD_TIME = 600;
int PENDING_TIME = 100;

// State machine
enum EDeviceState {
  STATE_NONE, STATE_BLOCKED, STATE_UNBLOCKING, STATE_PENDING, STATE_IDLE, STATE_OPEN, STATE_CLOSED = STATE_IDLE
};

EDeviceState state;
unsigned long timeBlocking, timeLastAction, timeAutoblocking;

EDeviceState newState = STATE_NONE;
bool error = false;
///////////////////////////////////
#ifndef LOCAL_RUN
IPAddress network_ip(192, 168, 31, 201);
IPAddress network_gateway(192, 168, 31, 1);
IPAddress network_subnet(255, 255, 255, 0);

ESP8266WebServer server(80);

void WIFIinit() {
  WiFi.mode(WIFI_STA);
  byte tries = 51;
  WiFi.begin(WiFi_login, WiFi_password);
  while (--tries && WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();

  if (WiFi.status() != WL_CONNECTED) {
    StartAPMode();
  } else {
    WiFi.config(network_ip, network_gateway, network_subnet);
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
  }
}

void StartAPMode() {
  WiFi.disconnect();
  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(network_ip, network_ip, network_subnet);
  WiFi.softAP(WiFi_login, WiFi_password);

  Serial.print("AP IP address: ");
  Serial.println(WiFi.softAPIP());
}
#endif

const int BLOCKING_PIN = 16;
const int VALVE_PIN = 4;
/////////////////////////////////////////
void deviceInfo() {
  #ifndef LOCAL_RUN
  String s = "{";
  s += "\"online\":true,";
  s += "\"error\":" + String(error) + ",";
  s += "\"state\":" + String((int)state) + ",";
  s += "\"CLOSE_DELAY\":" + String(CLOSE_DELAY) + ",";
  s += "\"BLOCK_DELAY\":" + String(BLOCK_DELAY) + ",";
  s += "\"BLOCKING_HOLD_TIME\":" + String(BLOCKING_HOLD_TIME) + ",";
  s += "\"PENDING_TIME\":" + String(PENDING_TIME);
  s += "}";

  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.send(200, "application/json", s);
  #endif
}

void configure() {
  #ifndef LOCAL_RUN
  if(server.hasArg("close_delay")) {
    CLOSE_DELAY = server.arg("close_delay").toInt();
  }

  if(server.hasArg("block_delay")) {
    BLOCK_DELAY = server.arg("block_delay").toInt();
  }

  if(server.hasArg("blocking_time")) {
    BLOCKING_HOLD_TIME = server.arg("blocking_time").toInt();
  }

  if(server.hasArg("pending_time")) {
    PENDING_TIME = server.arg("pending_time").toInt();
  }

  deviceInfo();
  #endif
}

void emptyResponse() {
  #ifndef LOCAL_RUN
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.send(200, "application/json", "{}");
  #endif
}

/////////////////////////////////////////
void setup() {
  pinMode(BLOCKING_PIN, OUTPUT);
  pinMode(VALVE_PIN, OUTPUT);
  digitalWrite(BLOCKING_PIN, HIGH);
  digitalWrite(VALVE_PIN, HIGH);
  state = STATE_BLOCKED;

  #ifndef LOCAL_RUN
  Serial.begin(115200);
  WIFIinit();
  wifi_set_sleep_type(NONE_SLEEP_T);

  server.on("/", HTTP_GET, deviceInfo);
  server.on("/open", HTTP_GET, openRequest);
  server.on("/close", HTTP_GET, closeRequest);
  server.on("/config", HTTP_GET, configure);

  server.begin();
  Serial.println("HTTP server started");
  #endif
}

void openRequest(int closeDelay) {
  CLOSE_DELAY = closeDelay;
  newState = STATE_OPEN;
  emptyResponse();
}

void openRequest() {
  #ifndef LOCAL_RUN
  if(server.hasArg("close_delay")) {
    openRequest(server.arg("close_delay").toInt());
    return;
  }
  #endif
  openRequest(CLOSE_DELAY);
}

void closeRequest() {
  newState = STATE_CLOSED;
  emptyResponse();
}

void loop(void) {
  #ifndef LOCAL_RUN
  server.handleClient();
  #endif
  // Заблокировано. Можем перейти в режим разблокировки и ПОТОМ Открыт
  if (STATE_BLOCKED == state) {
    digitalWrite(BLOCKING_PIN, HIGH);
    digitalWrite(VALVE_PIN, HIGH);
    if (STATE_OPEN == newState) {
      state = STATE_UNBLOCKING;
      timeBlocking = millis();
      timeAutoblocking = millis();
    }
    return;
  }
  //////////////////////////////
  // Разблокировка.
  if (STATE_UNBLOCKING == state) {
    digitalWrite(BLOCKING_PIN, LOW);
    digitalWrite(VALVE_PIN, HIGH); // ПЕРЕКРЫВАЕМ ВОДУ.
    if (BLOCKING_HOLD_TIME < (millis() - timeBlocking)) {
      state = STATE_PENDING;
      timeBlocking = millis();
    }
    return;
  }
  // Пауза после разблокировки.
  if (STATE_PENDING == state) {
    digitalWrite(BLOCKING_PIN, HIGH);
    digitalWrite(VALVE_PIN, HIGH); // ПЕРЕКРЫВАЕМ ВОДУ.
    if (PENDING_TIME < (millis() - timeBlocking)) {
      state = STATE_IDLE;
      timeBlocking = millis();
    }
    return;
  }
  //////////////////////////////
  if (STATE_NONE != newState) {
    state = newState;
    newState = STATE_NONE;
    timeLastAction = timeAutoblocking = millis();
    return;
  }

  // Режим ожидания. Можно открыть
  if (STATE_IDLE == state) {
    digitalWrite(BLOCKING_PIN, HIGH);
    digitalWrite(VALVE_PIN, HIGH); // ПЕРЕКРЫВАЕМ ВОДУ.
    // Блокировка через N сек
    if (BLOCK_DELAY < (millis() - timeAutoblocking)) {
      state = STATE_BLOCKED;
    }
    return;
  }
  //////////////////////////////
  // Кран открыт
  if (STATE_OPEN == state) {
    digitalWrite(BLOCKING_PIN, HIGH);
    digitalWrite(VALVE_PIN, LOW); // отКРЫВАЕМ ВОДУ.

    timeAutoblocking = millis();
    // Закрытие через N сек
    if (CLOSE_DELAY < (millis() - timeLastAction)) {
      state = STATE_CLOSED;
    }
    return;
  }
  // Что-то не то
  error = true;
}
